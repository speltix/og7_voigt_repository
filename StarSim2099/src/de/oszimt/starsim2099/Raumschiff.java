package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff extends RaumobjektXY{

	private String typ;
	private String antrieb;
	private int maxKapazit�t;
	private int winkel;

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getAntrieb() {
		return antrieb;
	}

	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}

	public int getMaxLadekapazitaet() {
		return maxKapazit�t;
	}

	public void setMaxLadekapazitaet(int maxKapazit�t) {
		this.maxKapazit�t = maxKapazit�t;
	}

	public int getWinkel() {
		return winkel;
	}

	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}
	
	// Attribute

	// Methoden

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { { '\0', '\0', '_', '\0', '\0' }, { '\0', '/', 'X', '\\', '\0' },
				{ '\0', '{', 'X', '}', '\0' }, { '\0', '{', 'X', '}', '\0' }, { '/', '_', '_', '_', '\\' }, };
		return raumschiffShape;
	}
}
