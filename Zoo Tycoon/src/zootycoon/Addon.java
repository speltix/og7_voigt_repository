package zootycoon;
// Autor Christian Voigt
public class Addon {

	private int idNummer;
	private String bezeichnung;
	private double preis;
	private int maxbestandpp; //pp = pro Player
	private int bestand;
	
	public Addon(int idNummer, String bezeichnung, double preis, int maxbestandpp, int bestand) {
		super();
		this.idNummer = idNummer;
		this.bezeichnung = bezeichnung;
		this.preis = preis;
		this.maxbestandpp = maxbestandpp;
		this.bestand = bestand;
	}

	public int getIdNummer() {
		return idNummer;
	}

	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getMaxbestandpp() {
		return maxbestandpp;
	}

	public void setMaxbestandpp(int maxbestandpp) {
		this.maxbestandpp = maxbestandpp;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}
	public void verkaufen(int menge) {
		this.bestand = this.bestand + menge;
	}
	public void kaufen(int menge) {
		this.bestand = this.bestand - menge;
	}

	@Override
	public String toString() {
		return "Addon [idNummer=" + idNummer + ", bezeichnung=" + bezeichnung + ", preis=" + preis + ", maxbestandpp="
				+ maxbestandpp + ", bestand=" + bestand + "]";
	}
}
