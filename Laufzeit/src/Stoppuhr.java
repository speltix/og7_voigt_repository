
public class Stoppuhr {
	private long start=0;
	private long stopp=0;
	
	
	public Stoppuhr() {
		super();
	}
	public void start() {
		this.start=System.nanoTime();
	}
	public void stopp() {
		this.stopp=System.nanoTime();
	}
	public void reset() {
		this.start=0;
		this.stopp=0;
	}
	public long getZeit() {
		return this.stopp-this.start;
	}

}
