import java.util.Random;

public class Linear {
	public final static int NICHT_GEFUNDEN = -1;
	
	public static void main(String[] args) {
		Stoppuhr zeit = new Stoppuhr();
		for (int i = 20000000; i >= 15000000; i-=1000000) {
			zeit.reset();
			long[] zahlen = getSortedList(i);
			
			System.out.println("Listenlšnge."+i);
			//best case
			System.out.println("best case");
			zeit.start();
			System.out.println("Zahl "+ zahlen[0] + " ist an Stelle "+ linear(zahlen,zahlen[1])+ " in der Liste.(-1=nicht vorhanden)");
			zeit.stopp();
			System.out.println("Gebrauchte Zeit (in Nanosekunden): "+zeit.getZeit());
			zeit.reset();
			
			//avarage case
			System.out.println("avarage case");
			zeit.start();
			System.out.println("Zahl "+ zahlen[zahlen.length/2] + " ist an Stelle "+ linear(zahlen,zahlen[zahlen.length/2])+ " in der Liste.(-1=nicht vorhanden)");
			zeit.stopp();
			System.out.println("Gebrauchte Zeit (in Nanosekunden): "+zeit.getZeit());
			zeit.reset();
			
			//worst case
			System.out.println("worst case");
			zeit.start();
			System.out.println("Zahl 111112 ist an Stelle "+ linear(zahlen,111112)+ " in der Liste.(-1=nicht vorhanden)");
			zeit.stopp();
			System.out.println("Gebrauchte Zeit (in Nanosekunden): "+zeit.getZeit());
		}
	}
	public static long[] getSortedList(int laenge){
		Random rand = new Random(111111);
		long[] zahlenliste = new long[laenge];
		long naechsteZahl = 0;
		
		for(int i = 0; i < laenge; i++){
			naechsteZahl += rand.nextInt(3)+1;
			zahlenliste[i] = naechsteZahl;
		}
		
		return zahlenliste;
	}
	public static int linear(long[] zahlen, long zahl) {
		for (int i = 0; i < zahlen.length; i++) {
			if(zahlen[i]==zahl) {
				return i;
			}
		}
		return NICHT_GEFUNDEN;
	}
}
