import java.util.Random;

public class Binaer {
	public final static int NICHT_GEFUNDEN = -1;
	
	public static void main(String[] args) {
		Stoppuhr zeit = new Stoppuhr();
		for (int i = 15000000; i <= 20000000; i+= 1000000) {
			zeit.reset();
			long[] zahlen = getSortedList(i);
			
			System.out.println("Listenlšnge."+i);
			//best case
			System.out.println("best case");
			zeit.start();
			System.out.println("Zahl "+ zahlen[zahlen.length/2] + " ist an Stelle "+ binaer(zahlen,zahlen[zahlen.length/2],0,zahlen.length)+ " in der Liste.(-1=nicht vorhanden)");
			zeit.stopp();
			System.out.println("Gebrauchte Zeit (in Nanosekunden): "+zeit.getZeit());
			zeit.reset();
			
			//avarage case
			System.out.println("avarage case");
			zeit.start();
			System.out.println("Zahl "+ zahlen[(int) (6*Math.log10(i+1))] + " ist an Stelle "+ binaer(zahlen,zahlen[(int) (6*Math.log10(i+1))],0,zahlen.length)+ " in der Liste.(-1=nicht vorhanden)");
			zeit.stopp();
			System.out.println("Gebrauchte Zeit (in Nanosekunden): "+zeit.getZeit());
			zeit.reset();
			
			//worst case
			System.out.println("worst case");
			zeit.start();
			System.out.println("Zahl 111112 ist an Stelle "+ binaer(zahlen,111112,0,zahlen.length)+ " in der Liste.(-1=nicht vorhanden)");
			zeit.stopp();
			System.out.println("Gebrauchte Zeit (in Nanosekunden): "+zeit.getZeit());
		}
	}
	public static long[] getSortedList(int laenge){
		Random rand = new Random(111111);
		long[] zahlenliste = new long[laenge];
		long naechsteZahl = 0;
		
		for(int i = 0; i < laenge; i++){
			naechsteZahl += rand.nextInt(3)+1;
			zahlenliste[i] = naechsteZahl;
		}
		return zahlenliste;
	}
	 public static long binaer(long[] zahlen, long zahl, int anfang, int ende) {
	        int m = (anfang+ende) / 2;
	        if (anfang>ende) {
	            return NICHT_GEFUNDEN;
	        } 
	         else if ( zahlen[m] == zahl) {
	            return m;
	        }
	        else if ( zahlen[m] > zahl) {
	            return binaer (zahlen, zahl, anfang, m-1 );
	    	}else  if ( zahlen[m] < zahl) {
	    		return binaer(zahlen, zahl, m+1, ende);
	    	}
	        return NICHT_GEFUNDEN;
	}


}
