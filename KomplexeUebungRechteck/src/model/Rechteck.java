package model;

public class Rechteck {

	private int x;
	private int y;
	private int breite;
	private int hoehe;
	
	public static void main(String[] args) {
		
	}

	public Rechteck() {
		
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.x = 0;
		this.y = 0;
		this.breite = 0;
		this.hoehe = 0;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}
	
	@Override
	public String toString() {
		return "Rechteck [>--  x = " + x + ", y = " + y + ", Breite = " + breite + ", H�he = " + hoehe + " --<]";            
	}
	
	
}
