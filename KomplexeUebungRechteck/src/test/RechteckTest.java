package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {
		Rechteck r0 = new Rechteck();
		r0.setX(10);
		r0.setY(10);
		r0.setBreite(30);
		r0.setHoehe(40);
		Rechteck r1 = new Rechteck();
		r1.setX(25);
		r1.setY(25);
		r1.setBreite(100);
		r1.setHoehe(20);
		Rechteck r2 = new Rechteck();
		r2.setX(260);
		r2.setY(10);
		r2.setBreite(200);
		r2.setHoehe(100);
		Rechteck r3 = new Rechteck();
		r3.setX(5);
		r3.setY(500);
		r3.setBreite(300);
		r3.setHoehe(25);
		Rechteck r4 = new Rechteck();
		r4.setX(100);
		r4.setY(100);
		r4.setBreite(100);
		r4.setHoehe(100);
		Rechteck r5 = new Rechteck(200,200,200,200);
		Rechteck r6 = new Rechteck(800,400,20,20);
		Rechteck r7 = new Rechteck(800,450,20,20);
		Rechteck r8 = new Rechteck(850,400,20,20);
		Rechteck r9 = new Rechteck(855,455,25,25);
		
		BunteRechteckeController controller1 = new BunteRechteckeController();
		controller1.add(r0);
		controller1.add(r1);
		controller1.add(r2);
		controller1.add(r3);
		controller1.add(r4);
		controller1.add(r5);
		controller1.add(r6);
		controller1.add(r7);
		controller1.add(r8);
		controller1.add(r9);
		
		System.out.println(r0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"));
		System.out.println(controller1.toString());
	}

}
