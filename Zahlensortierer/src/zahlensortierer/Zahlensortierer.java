package zahlensortierer;

import java.util.*;


/**
 * @author Christian Voigt
 *
 */
public class Zahlensortierer {

	private int zahl1;
	private int zahl2;
	private int zahl3;
	 
	// vervollst�ndigen Sie die main methode so, dass sie 3 Zahlen vom Benutzer 
	// einliest und die kleinste und gr��te Zahl ausgibt.
	public static void main(String[] args) {
		

		Scanner myScanner = new Scanner(System.in);

		System.out.print("1. Zahl: ");
		int zahl1 = myScanner.nextInt();
		
		System.out.print("2. Zahl: ");
		int zahl2 = myScanner.nextInt();
		
		System.out.print("3. Zahl: ");
		int zahl3 = myScanner.nextInt();
		
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("\nDie gr��te Zahl ist: " + zahl1);
		}
		
		if (zahl2 > zahl1 && zahl2 > zahl3) {
			System.out.println("\nDie gr��te Zahl ist: " + zahl2);
		}
		
		if (zahl3 > zahl1 && zahl3 > zahl2) {
			System.out.println("\nDie gr��te Zahl ist: " + zahl3);
		}
		
		if (zahl1 < zahl2 && zahl1 < zahl3) {
			System.out.println("Die kleinste Zahl ist: " + zahl1);
		}
		
		if (zahl2 < zahl1 && zahl2 < zahl3) {
			System.out.println("Die kleinste Zahl ist: " + zahl2);
		}
		
		if (zahl3 < zahl1 && zahl3 < zahl2) {
			System.out.println("Die kleinste Zahl ist: " + zahl3);
		}
		
		
		myScanner.close();
	}
}
