package keystore;

import java.util.Arrays;

public class KeyStore01 {
	int stelle=0;
	String[] key;
	
	public KeyStore01() {
		key = new String[100];
	}
	public KeyStore01(int length) {
		key = new String[length];
	}
	public boolean add(String eintrag) {
		if (stelle<key.length) {
			key[stelle]=eintrag;
			stelle++;
			return true;
		}
		return false;
	}
	public int indexOf(String eintrag) {
		for (int i = 0; i < key.length; i++) {
			if (eintrag==key[i]) {
				return i;
			}
		}
		return -1;
	}
	public void remove(int index) {
		key[index]=null;
		for (int i = index; i < key.length-1; i++) {
			key[i] = key[i+1];
		}
		key[key.length-1]=null;
		}
	
	public boolean remove(String eintrag) {
		int index = indexOf(eintrag);
		if(index>=0) {
			key[index]=null;
			return true;
		}
		return false;
	}
	@Override
	public String toString() {
		String s = "";
		for(int i = 0;i < key.length; i++) {
			if(this.key[i] != null) {
				s += key[i] + "\n";
			}
		}
		return s;
		
	}
	public void clear() {
		for (int i = 0;i < key.length; i++) {
			key[i]=null;
		}
		stelle=0;
	}
	public int size() {
		return key.length;
	}
	public String get(int index) {
		return key[index];
	}

}
