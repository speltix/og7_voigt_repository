package uebungarray;

import java.util.Scanner;

public class Array1 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int[] array = new int[5];
		{
			int i = 0;
			int p = 0;
			while (p < array.length) {
				i = scan.nextInt();
				array[p] = i;
				p++;
			}
			for (int r = 0; r < array.length; r++) {
				System.out.print(array[r] + ",");
			}
		}
	}
}