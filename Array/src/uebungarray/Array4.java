package uebungarray;

import java.util.Scanner;

public class Array4 {

	public static void main(String[] args) {
		System.out.print("Gr��e Array: ");
		Scanner scan = new Scanner(System.in);

		int x = scan.nextInt();
		int a[][] = new int[x][x];
		int ii = 0;
		for (int i = 0; i < x; i++) {
			a[ii][i] = i * ii;
			if (i == x - 1 && ii < x - 1) {
				ii++;
				i = -1;

			}
		}
		ii = 0;
		for (int i = 0; i < x; i++) {
			if (ii == x) {
				break;
			}
			System.out.print(a[ii][i]);
			if (i == x - 1 && ii < x) {
				ii++;
				i = -1;
				System.out.print("\n");
			} else
				System.out.print(",");
		}
		scan.close();
	}

}
