package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);

		if (obj == f.btnEinfuellen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = fuellstand + 5;
			if (fuellstand < 95) {
				f.myTank.setFuellstand(fuellstand);
			} else {
				f.myTank.setFuellstand(100);
			}
			f.lblprozent.setText(f.myTank.getFuellstand() + "%");
			f.lblFuellstand.setText("" + f.myTank.getFuellstand() + " L");
		}

		if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = fuellstand - 2;
			if (fuellstand > 1) {
				f.myTank.setFuellstand(fuellstand);
			} else {
				f.myTank.setFuellstand(0);
			}
			f.lblprozent.setText(f.myTank.getFuellstand() + "%");
			f.lblFuellstand.setText("" + f.myTank.getFuellstand() + " L");
		}
		if (obj == f.btnZurücksetzen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = 0;
			f.myTank.setFuellstand(fuellstand);

			f.lblFuellstand.setText("" + fuellstand + " L");

		}
	}
}