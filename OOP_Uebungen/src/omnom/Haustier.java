package omnom;

public class Haustier {

	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;
	public Haustier() {
		super();
	}
	public Haustier(String name) {
		super();
		this.hunger = 100;
		this.muede = 100;
		this.zufrieden = 100;
		this.gesund = 100;
		this.name = "Kotstulle";
	}
	public int getHunger() {
		return hunger;
	}
	public void setHunger(int hunger) {
		if (hunger <= 100) {
			this.hunger = hunger;
		}
		if (hunger > 100) {
			this.hunger = 100;
		}
		if (hunger <= 0) {
			this.hunger = 0;
		}
	}
	public int getMuede() {
		return muede;
	}
	public void setMuede(int muede) {
		if (muede <= 100) {
			this.muede = muede;
		}
		if (muede > 100) {
			this.muede = 100;
		}
		if (muede <= 0) {
			this.muede = 0;
		}
	}
	public int getZufrieden() {
		return zufrieden;
	}
	public void setZufrieden(int zufrieden) {
		if (zufrieden <= 100) {
			this.zufrieden = zufrieden;
		}
		if (zufrieden > 100) {
			this.zufrieden = 100;
		}
		if (zufrieden <= 0) {
			this.zufrieden = 0;
		}
	}
	public int getGesund() {
		return gesund;
	}
	public void setGesund(int gesund) {
		if(gesund <= 100) {
			this.gesund = gesund;
		}
		if (gesund > 100) {
			this.gesund = 100;
		}
		if (gesund <= 0) {
			this.gesund = 0;
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void fuettern(int i) {
		this.hunger += 30;
	}
	public void schlafen(int i) {
		this.muede += 100;
	}
	public void spielen(int i) {
		this.zufrieden += 20;
	}
	public void heilen() {
		this.gesund += 100;
	}
	
}
