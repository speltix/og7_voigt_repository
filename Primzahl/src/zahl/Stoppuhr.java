package zahl;

public class Stoppuhr {
	
	private long start;
	private long stop;
	
	public void starte() {
		start = System.nanoTime();
	}
	
	public void stoppe() {
		stop = System.nanoTime();
	}
	
	public long lies() {
		return Math.abs(stop - start);
	}

}
