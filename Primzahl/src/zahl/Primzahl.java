package zahl;

import java.util.Scanner;

public class Primzahl {

	public static void main(String[] args) {


		Stoppuhr t = new Stoppuhr();
		Zahlenmethode z = new Zahlenmethode();

		long[] primes = {
				7l, 
				97l,
				997l,
				9973l,
				99929l,
				999983l,
				9999991l,
				99999989l,	
				999999937l,
				9999999929l,     // 10
				99999999977l,	 // 11
				999999000001l,   // 12
				9999999998987l,  // 13
				99999999944441l, // 14
				999998727899999l // 15
				};//, 8888888897888888899l };		
		long[][] measurement = new long[primes.length][10];
		boolean[][] isprime = new boolean[primes.length][10];

		t.starte();
		for (int i = 0; i < primes.length; i++) {
			// Primzahl i
			System.out.format("Testing prime %d [%d/%d]%n", primes[i], i, primes.length);
			for (int j = 0; j < measurement[i].length; j++) {
				// Messung j
				t.starte();
				isprime[i][j] = Zahlenmethode.isPrimzahl(primes[i]);
				t.stoppe();
				measurement[i][j] = t.lies();
				System.out.format("Measurement Prime %d [%d/%d]: %02.2f ms%n", i, j, measurement[i].length, measurement[i][j]/1000000.0);
			}
		}
	}
}
