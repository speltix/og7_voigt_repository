import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.text.*;

public class Logic {

	public Logic(FileControl filecontrol) {
		this.reloadfromDiskOld();
	}

	private ArrayList<Lerneintrag> lerneintraege = new ArrayList<Lerneintrag>();

	public static ArrayList<String> datumAktuell = new ArrayList<String>();
	public static ArrayList<String> faecherAktuell = new ArrayList<String>();
	public static ArrayList<String> aktivitaetenAktuell = new ArrayList<String>();
	public static ArrayList<String> dauerAktuell = new ArrayList<String>();

	public static void reloadfromDisk() {
		FileControl.einlesen(FileControl.dateiNameFuerReload);
		Collections.copy(datumAktuell, FileControl.datum);
		Collections.copy(faecherAktuell, FileControl.faecher);
		Collections.copy(aktivitaetenAktuell, FileControl.aktivitaeten);
		Collections.copy(dauerAktuell, FileControl.dauer);
	}

	public void reloadfromDiskOld() {
		for (int j = 0; j < FileControl.datum.size(); j++) {
			SimpleDateFormat format = new SimpleDateFormat("DD.MM.YYYY");
			try {
				this.lerneintraege.add(new Lerneintrag(format.parse(FileControl.datum.get(j)), FileControl.faecher.get(j),FileControl.aktivitaeten.get(j), Integer.valueOf(FileControl.dauer.get(j))));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (java.text.ParseException e) {
				e.printStackTrace();
			}
		}
	}

	public static String createReport() {
		int gesamtdauer = 0;
		for (int i = 0; i < FileControl.dauer.size(); i++) {
			gesamtdauer += Integer.valueOf(FileControl.dauer.get(i));
		}
		int durchschnitt = gesamtdauer / FileControl.dauer.size();
		return gesamtdauer + "," + durchschnitt + "," + FileControl.dauer.size();
	}

	public static String getLearnerName() {
		return FileControl.ergebnis[0];
	}

	public boolean addEntry(Lerneintrag l) {
		boolean iserfolgreich = FileControl.addEntry(l);
		if (iserfolgreich == true) {
			this.lerneintraege.add(l);
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean addEntryNeu(String datum, String fach, String aktivitaet, String dauer) {
		boolean isErfolgreich = FileControl.addEntryNeu(datum,fach,aktivitaet,dauer);
		if (isErfolgreich == true) {
			FileControl.datum.add(datum);
			Logic.datumAktuell.add(datum);
			FileControl.faecher.add(fach);
			Logic.faecherAktuell.add(fach);
			FileControl.aktivitaeten.add(aktivitaet);
			Logic.aktivitaetenAktuell.add(aktivitaet);
			FileControl.dauer.add(dauer);
			Logic.dauerAktuell.add(dauer);
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<Lerneintrag> getListEntries() {
		return this.lerneintraege;
	}

}
