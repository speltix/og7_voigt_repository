import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class FileControl {

	public static String dateiNameFuerReload = "Daten.txt";
	public static String ausgabe = "";
	public static String zeile = "";
	public static String[] ergebnis;
	public static ArrayList<String> datum = new ArrayList<String>();
	public static ArrayList<String> faecher = new ArrayList<String>();
	public static ArrayList<String> aktivitaeten = new ArrayList<String>();
	public static ArrayList<String> dauer = new ArrayList<String>();
	public static ArrayList<String> gui = new ArrayList<String>();

	public static void einlesen(String dateiName) {
		try {
			dateiNameFuerReload = dateiName;
			BufferedReader br = new BufferedReader(new FileReader(new File(dateiName)));

			while ((zeile = br.readLine()) != null) {
				ausgabe += zeile + "\n";
			}
			ergebnis = ausgabe.split("\n");
			for (int i = 1, j = 1; i < ergebnis.length; i++, j++) {
				if (j == 5) {
					j = 1;
				}
				if (j == 1) {
					datum.add(ergebnis[i]);
				} else if (j == 2) {
					faecher.add(ergebnis[i]);
				} else if (j == 3) {
					aktivitaeten.add(ergebnis[i]);
				} else if (j == 4) {
					dauer.add(ergebnis[i]);
				}
			}
			System.out.println("Lerntagebuch von " + ergebnis[0]);
			String format = "| %-12s | %-8s | %-37s | %-5s |%n";
			System.out.format("+--------------+----------+---------------------------------------+-------+%n");
			System.out.format("| Datum        | Fach     | Aktivitšt                             | Dauer |%n");
			System.out.format("+--------------+----------+---------------------------------------+-------+%n");
			for (int i = 0; i < datum.size(); i++) {
				System.out.format(format, datum.get(i), faecher.get(i), aktivitaeten.get(i), dauer.get(i));
			}
			System.out.format("+--------------+----------+---------------------------------------+-------+%n");
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public static void clear() {
		for (int i = 0; i < ergebnis.length; i++) {
			ergebnis[i] = null;
		}
		datum.clear();
		faecher.clear();
		aktivitaeten.clear();
		dauer.clear();
		gui.clear();
		ausgabe = "";
		zeile = "";
	}

	public static boolean addEntry(Lerneintrag l) {
		try {
			PrintWriter printWriter = new PrintWriter(new FileWriter(dateiNameFuerReload, true));
			SimpleDateFormat format = new SimpleDateFormat("DD.MM.YYYY");
			printWriter.write(format.format(l.getDatum()) + "\n");
			printWriter.write(l.getFach() + "\n");
			printWriter.write(l.getBeschreibung() + "\n");
			printWriter.write(l.getDauer() + "\n");
			printWriter.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean addEntryNeu(String datum, String fach, String aktivitaet, String dauer) {
		try {
			PrintWriter printWriter = new PrintWriter(new FileWriter(dateiNameFuerReload, true));
			printWriter.write(datum + "\n");
			printWriter.write(fach + "\n");
			printWriter.write(aktivitaet + "\n");
			printWriter.write(dauer + "\n");
			printWriter.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

}
