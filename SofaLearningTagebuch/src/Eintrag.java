import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class Eintrag extends JFrame {

	private JPanel contentPane;
	private JTextField txtF_Datum;
	private JTextField txtF_Fach;
	private JTextField txtF_Dauer;
	private GUI gui;

	/**
	 * Create the frame.
	 */
	public Eintrag(GUI gui) {
		this.gui = gui;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 496, 333);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtF_Datum = new JTextField();
		txtF_Datum.setBounds(188, 68, 160, 20);
		contentPane.add(txtF_Datum);
		txtF_Datum.setColumns(10);

		txtF_Fach = new JTextField();
		txtF_Fach.setBounds(188, 111, 160, 20);
		contentPane.add(txtF_Fach);
		txtF_Fach.setColumns(10);

		JTextField txtF_Aktivitaet = new JTextField();
		txtF_Aktivitaet.setBounds(188, 157, 160, 20);
		contentPane.add(txtF_Aktivitaet);
		txtF_Aktivitaet.setColumns(10);

		txtF_Dauer = new JTextField();
		txtF_Dauer.setBounds(188, 201, 160, 20);
		contentPane.add(txtF_Dauer);
		txtF_Dauer.setColumns(10);

		JLabel lbl_Datum = new JLabel("Datum:");
		lbl_Datum.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl_Datum.setBounds(51, 69, 97, 14);
		contentPane.add(lbl_Datum);

		JLabel lbl_Fach = new JLabel("Fach:");
		lbl_Fach.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl_Fach.setBounds(51, 112, 97, 14);
		contentPane.add(lbl_Fach);

		JLabel lbl_Aktivitaet = new JLabel("Aktivitšt:");
		lbl_Aktivitaet.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl_Aktivitaet.setBounds(51, 158, 97, 14);
		contentPane.add(lbl_Aktivitaet);

		JLabel lbl_Dauer = new JLabel("Dauer:");
		lbl_Dauer.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl_Dauer.setBounds(51, 202, 97, 14);
		contentPane.add(lbl_Dauer);

		JLabel lblNewLabel_4 = new JLabel("Lerneintrag ");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_4.setBounds(166, 21, 136, 20);
		contentPane.add(lblNewLabel_4);

		JButton btnSpeichern = new JButton("Speichern");
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String datum = txtF_Datum.getText();
				String fach = txtF_Fach.getText();
				String aktivitaet = txtF_Aktivitaet.getText();
				String dauer = txtF_Dauer.getText();
				boolean ergebnis = Logic.addEntryNeu(datum, fach, aktivitaet, dauer);
				GUI.aktualisieren(ergebnis);
				txtF_Datum.setText("");
				txtF_Fach.setText("");
				txtF_Aktivitaet.setText("");
				txtF_Dauer.setText("");
			}
		});
		btnSpeichern.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnSpeichern.setBounds(23, 250, 115, 33);
		contentPane.add(btnSpeichern);

		JButton btnSpeichernalt = new JButton("SpeichernAlt");
		btnSpeichernalt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String datum = txtF_Datum.getText();
				String fach = txtF_Fach.getText();
				String aktivitaet = txtF_Aktivitaet.getText();
				String dauer = txtF_Dauer.getText();
				SimpleDateFormat format2 = new SimpleDateFormat("DD.MM.YYYY");
				try {
					Lerneintrag neu = new Lerneintrag(format2.parse(datum), fach, aktivitaet, Integer.valueOf(dauer));
					boolean ergebnis = gui.logic.addEntry(neu);
					GUI.aktualisierenAlt(ergebnis);
				} catch (Exception e) {
					e.printStackTrace();
				}
				txtF_Datum.setText("");
				txtF_Fach.setText("");
				txtF_Aktivitaet.setText("");
				txtF_Dauer.setText("");
			}
		});
		btnSpeichernalt.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnSpeichernalt.setBounds(148, 250, 136, 33);
		contentPane.add(btnSpeichernalt);

		JButton btnNewButton = new JButton("Schlie\u00DFen");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtF_Datum.setText("");
				txtF_Fach.setText("");
				txtF_Aktivitaet.setText("");
				txtF_Dauer.setText("");
				gui.eintragExit();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(303, 250, 115, 33);
		contentPane.add(btnNewButton);

		JLabel lbl_DatumForm = new JLabel("14.12.2017");
		lbl_DatumForm.setBounds(358, 71, 97, 14);
		contentPane.add(lbl_DatumForm);

		JLabel lbl_FachForm = new JLabel("IT");
		lbl_FachForm.setBounds(358, 114, 46, 14);
		contentPane.add(lbl_FachForm);

		JLabel lblNewLabel_2 = new JLabel("Beispiele:");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_2.setBounds(358, 45, 60, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lbl_AktivitaetForm = new JLabel("O-Notation");
		lbl_AktivitaetForm.setBounds(358, 160, 112, 14);
		contentPane.add(lbl_AktivitaetForm);

		JLabel lbl_DauerForm = new JLabel("75");
		lbl_DauerForm.setBounds(358, 204, 46, 14);
		contentPane.add(lbl_DauerForm);
	}
}
