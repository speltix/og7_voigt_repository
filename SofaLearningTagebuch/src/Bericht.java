import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Bericht extends JFrame {

	private JPanel contentPane;
	private static JLabel lbl_GesamtWert;
	private static JLabel lbl_DurchschnittWert;
	private static JLabel lbl_AnzahlLernaktivitaetenErgebnis;

	/**
	 * Create the frame.
	 */
	public Bericht() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 426, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lbl_GesamtWert = new JLabel("");
		lbl_GesamtWert.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl_GesamtWert.setBounds(239, 70, 125, 17);
		contentPane.add(lbl_GesamtWert);

		JLabel lbl_Gesamt = new JLabel("Gesamt-Lernzeit:");
		lbl_Gesamt.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl_Gesamt.setBounds(24, 70, 125, 17);
		contentPane.add(lbl_Gesamt);

		JLabel lbl_Titel = new JLabel("Bericht zum Lerntagebuch von ");
		lbl_Titel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lbl_Titel.setBounds(39, 11, 252, 24);
		contentPane.add(lbl_Titel);

		JLabel lbl_Name = new JLabel(FileControl.ergebnis[0]);
		lbl_Name.setFont(new Font("Tahoma", Font.BOLD, 16));
		lbl_Name.setBounds(291, 14, 143, 19);
		contentPane.add(lbl_Name);

		JLabel lbl_Durchschnitt = new JLabel("Durchschnitt-Lernzeit:");
		lbl_Durchschnitt.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl_Durchschnitt.setBounds(24, 121, 176, 14);
		contentPane.add(lbl_Durchschnitt);

		lbl_DurchschnittWert = new JLabel("");
		lbl_DurchschnittWert.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl_DurchschnittWert.setBounds(239, 121, 141, 14);
		contentPane.add(lbl_DurchschnittWert);

		JButton btn_Schliessen = new JButton("Schlie\u00DFen");
		btn_Schliessen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUI.berichtExit();
			}
		});
		btn_Schliessen.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn_Schliessen.setBounds(154, 217, 115, 33);
		contentPane.add(btn_Schliessen);

		JLabel lblAnzahlDerLernaktivitaeten = new JLabel("Anzahl der Lernaktivitaeten:");
		lblAnzahlDerLernaktivitaeten.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAnzahlDerLernaktivitaeten.setBounds(24, 162, 205, 17);
		contentPane.add(lblAnzahlDerLernaktivitaeten);

		lbl_AnzahlLernaktivitaetenErgebnis = new JLabel("");
		lbl_AnzahlLernaktivitaetenErgebnis.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl_AnzahlLernaktivitaetenErgebnis.setBounds(239, 165, 149, 14);
		contentPane.add(lbl_AnzahlLernaktivitaetenErgebnis);
	}

	public static void erstelleBericht() {
		String[] zeiten = Logic.createReport().split(",");
		lbl_GesamtWert.setText(zeiten[0]);
		lbl_DurchschnittWert.setText(zeiten[1]);
		lbl_AnzahlLernaktivitaetenErgebnis.setText(zeiten[2]);
	}

}
