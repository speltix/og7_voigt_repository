import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.TextArea;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;

public class GUI extends JFrame {

	private JPanel contentPane;
	public static Object[] titel = { "Datum", "Fach", "Aktivitaet", "Dauer" };
	public static Object[][] daten = {};
	public static Bericht unterfenster1;
	public static Eintrag unterfenster2;
	public static Logic logic = new Logic(new FileControl());
	private static TextArea txtA_Ausgabe;
	private static JTable table = new JTable(new DefaultTableModel(titel, 0));
	private JScrollPane scroll = new JScrollPane(table);
	private static DefaultTableModel model = (DefaultTableModel) table.getModel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		FileControl.einlesen("Daten.txt");
		try {
			GUI frame = new GUI(logic);
			unterfenster1 = new Bericht();
			unterfenster2 = new Eintrag(frame);
			frame.setVisible(true);
			unterfenster1.setVisible(false);
			unterfenster2.setVisible(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the frame.
	 */
	public GUI(Logic logic) {
		setResizable(false);
		this.logic = logic;
		setTitle("SofaLearning Lerntagebuch");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 901, 539);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtA_Ausgabe = new TextArea();
		txtA_Ausgabe.setFont(new Font("Monospaced", Font.PLAIN, 14));
		txtA_Ausgabe.setEditable(false);
		txtA_Ausgabe.setBounds(33, 67, 805, 180);
		contentPane.add(txtA_Ausgabe);

		JLabel lbl_Name = new JLabel(Logic.getLearnerName());
		lbl_Name.setFont(new Font("Tahoma", Font.BOLD, 24));
		lbl_Name.setBounds(254, 17, 284, 39);
		contentPane.add(lbl_Name);

		JLabel lbl_Header = new JLabel("Lerntagebuch von ");
		lbl_Header.setFont(new Font("Tahoma", Font.BOLD, 24));
		lbl_Header.setBounds(33, 11, 222, 50);
		contentPane.add(lbl_Header);

		JButton btn_NeuerEintrag = new JButton("Neuer Eintrag");
		btn_NeuerEintrag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!txtA_Ausgabe.getText().equals("")) {
					unterfenster2.setVisible(true);
				}
			}
		});
		btn_NeuerEintrag.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn_NeuerEintrag.setBounds(413, 450, 149, 30);
		contentPane.add(btn_NeuerEintrag);

		JButton btn_Bericht = new JButton("Bericht");
		btn_Bericht.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn_Bericht.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!txtA_Ausgabe.getText().equals("")) {
					unterfenster1.erstelleBericht();
					unterfenster1.setVisible(true);
				}
			}
		});
		btn_Bericht.setBounds(579, 450, 120, 30);
		contentPane.add(btn_Bericht);

		JButton btn_Beenden = new JButton("Schlie�en");
		btn_Beenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btn_Beenden.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn_Beenden.setBounds(718, 450, 120, 30);
		contentPane.add(btn_Beenden);

		scroll.setBounds(33, 253, 805, 186);
		contentPane.add(scroll);

		JButton btn_Miriam = new JButton("Miriam");
		btn_Miriam.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn_Miriam.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < FileControl.datum.size(); i++) {
					model.addRow(new Object[] { FileControl.datum.get(i), FileControl.faecher.get(i),
							FileControl.aktivitaeten.get(i), FileControl.dauer.get(i) });
				}
				String format2 = "| %-12s | %-8s | %-37s | %-5s |\n";
				StringWriter buffer = new StringWriter();
				PrintWriter writer = new PrintWriter(buffer);
				writer.format("+--------------+----------+---------------------------------------+-------+%n");
				writer.format("| Datum        | Fach     | Aktivit�t                             | Dauer |%n");
				writer.format("+--------------+----------+---------------------------------------+-------+%n");
				for (int i = 0; i < FileControl.datum.size(); i++) {
					writer.format(format2, FileControl.datum.get(i), FileControl.faecher.get(i),
							FileControl.aktivitaeten.get(i), FileControl.dauer.get(i));
				}
				String contents = buffer.toString();
				txtA_Ausgabe.append(contents);
			}
		});
		btn_Miriam.setBounds(33, 450, 89, 29);
		contentPane.add(btn_Miriam);

		JButton btn_AndereDatei = new JButton("Andere Datei");
		btn_AndereDatei.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();

				chooser.setCurrentDirectory(
						new File(System.getProperty("user.home") + System.getProperty("file.separator") + "Desktop"));

				chooser.showOpenDialog(null);
				File f = chooser.getSelectedFile();
				if (chooser.getTypeDescription(f).equals("txt")
						|| chooser.getTypeDescription(f).equals("Textdokument")) {

					FileControl.clear();
					FileControl.einlesen(f.getPath());
					if (model.getRowCount() > 0) {
						for (int i = model.getRowCount() - 1; i > -1; i--) {
							model.removeRow(i);
						}
					}
					for (int i = 0; i < FileControl.datum.size(); i++) {
						model.addRow(new Object[] { FileControl.datum.get(i), FileControl.faecher.get(i),
								FileControl.aktivitaeten.get(i), FileControl.dauer.get(i) });
					}
					String format2 = "| %-12s | %-8s | %-37s | %-5s |\n";
					StringWriter buffer = new StringWriter();
					PrintWriter writer = new PrintWriter(buffer);
					writer.format("+--------------+----------+---------------------------------------+-------+%n");
					writer.format("| Datum        | Fach     | Aktivit�t                             | Dauer |%n");
					writer.format("+--------------+----------+---------------------------------------+-------+%n");
					for (int i = 0; i < FileControl.datum.size(); i++) {
						writer.format(format2, FileControl.datum.get(i), FileControl.faecher.get(i),
								FileControl.aktivitaeten.get(i), FileControl.dauer.get(i));
					}
					String contents = buffer.toString();
					btn_Miriam.setText(logic.getLearnerName());
					lbl_Name.setText(logic.getLearnerName());
					txtA_Ausgabe.setText("");
					txtA_Ausgabe.append(contents);
				}
			}
		});
		btn_AndereDatei.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn_AndereDatei.setBounds(254, 451, 139, 29);
		contentPane.add(btn_AndereDatei);

		JButton btn_Alt = new JButton("Alt");
		btn_Alt.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn_Alt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logic.reloadfromDiskOld();
				ArrayList<Lerneintrag> liste = logic.getListEntries();
				for (int i = 0; i < liste.size(); i++) {
					txtA_Ausgabe.append(liste.get(i).toString());
				}
			}
		});
		btn_Alt.setBounds(156, 450, 68, 30);
		contentPane.add(btn_Alt);

	}

	public static void aktualisieren(boolean ergebnis) {
		if (ergebnis == true) {
			if (model.getRowCount() > 0) {
				for (int i = model.getRowCount() - 1; i > -1; i--) {
					model.removeRow(i);
				}
			}
			for (int i = 0; i < FileControl.datum.size(); i++) {
				model.addRow(new Object[] { FileControl.datum.get(i), FileControl.faecher.get(i),
						FileControl.aktivitaeten.get(i), FileControl.dauer.get(i) });
			}
			txtA_Ausgabe.setText("");
			String format2 = "| %-12s | %-8s | %-37s | %-5s |\n";
			StringWriter buffer = new StringWriter();
			PrintWriter writer = new PrintWriter(buffer);
			writer.format("+--------------+----------+---------------------------------------+-------+%n");
			writer.format("| Datum        | Fach     | Aktivit�t                             | Dauer |%n");
			writer.format("+--------------+----------+---------------------------------------+-------+%n");
			for (int i = 0; i < FileControl.datum.size(); i++) {
				writer.format(format2, FileControl.datum.get(i), FileControl.faecher.get(i),
						FileControl.aktivitaeten.get(i), FileControl.dauer.get(i));
			}
			String contents = buffer.toString();
			txtA_Ausgabe.append(contents);
			unterfenster2.setVisible(false);
		}
	}

	public static void aktualisierenAlt(boolean ergebnis) {
		if (ergebnis == true) {
			txtA_Ausgabe.setText("");
			ArrayList<Lerneintrag> liste = logic.getListEntries();
			for (int i = 0; i < liste.size(); i++) {
				txtA_Ausgabe.append(liste.get(i).toString());
			}
			unterfenster2.setVisible(false);
		}
	}

	public static void eintragExit() {
		unterfenster2.dispose();
	}

	public static void berichtExit() {
		unterfenster1.dispose();
	}

}
