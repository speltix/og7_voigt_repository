
public class Person {

	protected String name;
	protected String telefonnr;
	protected boolean jahresbetrag;
	
	public Person(String name, String telefonnr, boolean jahresbetrag) {
		super();
		this.name = name;
		this.telefonnr = telefonnr;
		this.jahresbetrag = jahresbetrag;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelefonnr() {
		return telefonnr;
	}
	public void setTelefonnr(String telefonnr) {
		this.telefonnr = telefonnr;
	}
	public boolean isJahresbetrag() {
		return jahresbetrag;
	}
	public void setJahresbetrag(boolean jahresbetrag) {
		this.jahresbetrag = jahresbetrag;
	}

	@Override
	public String toString() {
		return "Person [Name= " + name + ", Telefonnummer= " + telefonnr + ", Jahresbetrag gezahlt= " + jahresbetrag + "]";
	}
}
