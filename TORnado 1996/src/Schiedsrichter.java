
public class Schiedsrichter extends Person {

	private int anzgepfiffenerSpiele;

	public Schiedsrichter(String name, String telefonnr, boolean jahresbetrag, int anzgepfiffenerSpiele) {
		super(name, telefonnr, jahresbetrag);
		this.anzgepfiffenerSpiele = anzgepfiffenerSpiele;
	}

	public int getAnzgepfiffenerSpiele() {
		return anzgepfiffenerSpiele;
	}

	public void setAnzgepfiffenerSpiele(int anzgepfiffenerSpiele) {
		this.anzgepfiffenerSpiele = anzgepfiffenerSpiele;
	}

	@Override
	public String toString() {
		return "Schiedsrichter [Anzahl der gepfiffenen Spiele= " + anzgepfiffenerSpiele + ", Name= " + name + ", Telefonnummer= "
				+ telefonnr + ",Jahresbetrag gezahlt= " + jahresbetrag + "]";
	}

	
	
}
