
public class Spieler extends Person {

	
	private int trikotnr;
	private String pos;
	
	public Spieler(String name, String telefonnr, boolean jahresbetrag, int trikotnr, String pos) {
		super(name, telefonnr, jahresbetrag);
		this.trikotnr = trikotnr;
		this.pos = pos;
	}
	public int getTrikotnr() {
		return trikotnr;
	}
	public void setTrikotnr(int trikotnr) {
		this.trikotnr = trikotnr;
	}
	public String getPos() {
		return pos;
	}
	public void setPos(String pos) {
		this.pos = pos;
	}
	@Override
	public String toString() {
		return "Spieler [Trikotnummer= " + trikotnr + ", Position= " + pos + ", Name= " + name + ", Telefonnummer= " + telefonnr
				+ ", Jahresbetrag gezahlt= " + jahresbetrag + "]";
	}
	
}
