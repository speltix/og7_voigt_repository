
public class Trainer extends Person {

	private char lizensklasse;
	private int monatsaufwand;
	private char spielklasse;
	
	public Trainer(String name, String telefonnr, boolean jahresbetrag, char lizensklasse, int monatsaufwand,
			char spielklasse) {
		super(name, telefonnr, jahresbetrag);
		this.lizensklasse = lizensklasse;
		this.monatsaufwand = monatsaufwand;
		this.spielklasse = spielklasse;
	}
	public char getLizensklasse() {
		return lizensklasse;
	}
	public void setLizensklasse(char lizensklasse) {
		this.lizensklasse = lizensklasse;
	}
	public int getMonatsaufwand() {
		return monatsaufwand;
	}
	public void setMonatsaufwand(int monatsaufwand) {
		this.monatsaufwand = monatsaufwand;
	}
	public char getSpielklasse() {
		return spielklasse;
	}
	public void setSpielklasse(char spielklasse) {
		this.spielklasse = spielklasse;
	}
	@Override
	public String toString() {
		return "Trainer [Lizensklasse= " + lizensklasse + ", Monatsaufwand= " + monatsaufwand + "�" + ", Spielklasse= "
				+ spielklasse + ", Name= " + name + ", Telefonnummer= " + telefonnr + ", Jahresbetrag gezahlt= " + jahresbetrag + "]";
	}
	
	
}
