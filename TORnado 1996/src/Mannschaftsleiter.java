
public class Mannschaftsleiter extends Spieler {

	
	private boolean engagement;
	private boolean rabattjahresbetrag;
	
	public Mannschaftsleiter(String name, String telefonnr, boolean jahresbetrag, int trikotnr, String pos,
			boolean engagement, boolean rabattjahresbetrag) {
		super(name, telefonnr, jahresbetrag, trikotnr, pos);
		this.engagement = engagement;
		this.rabattjahresbetrag = rabattjahresbetrag;
	}
	public boolean isEngagement() {
		return engagement;
	}
	public void setEngagement(boolean engagement) {
		this.engagement = engagement;
	}
	public boolean isRabattjahresbetrag() {
		return rabattjahresbetrag;
	}
	public void setRabattjahresbetrag(boolean rabattjahresbetrag) {
		this.rabattjahresbetrag = rabattjahresbetrag;
	}
	@Override
	public String toString() {
		return "Mannschaftsleiter [Engagement= " + engagement + ", Rabattjahresbetrag= " + rabattjahresbetrag + ", Name= "
				+ name + ", Telefonnummer= " + telefonnr + ", Jahresbetrag gezahlt= " + jahresbetrag + "]";
	}
	
}
